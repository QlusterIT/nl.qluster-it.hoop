Bringing you H.O.O.P (Dutch for Hope).

Homey Object Oriented Programming (for advanced users).


This app transforms your Homey in to a programmable object, changing standard flow behavior.
Take a look at the Community Forum Topic for diagrams, examples and overall usages.


You need to grant ‘developer’ access rights’ to the app by creating a Homey Elevator device. If you get an error, use the Settings to grant access.

Create one (or more) Method Group device(s) that can be used in an Object Oriented Programming manner.
Give it (overloadable) methods which will handle their own objects (devices, flows, variables, etc), removing the need for repetitive flows.
Setting the Method Group Device Setting "Overloadable" will let you even trigger a TriggerCard (Text3:, Boolean1:) by a Condition/ActionCard (Text:2, Boolean1) or a Condition/ActionCard (Text:1).

Or trigger any flow as condition and await till its done, creating synchronised actions, handling variables before executing any further actions, all within the same flow.

Create a Flow Actions Device that allows the execution of Actioncards (Then-Cards) as Conditions and await them.

Also a Flow Director is available, giving you If, Or(else), And(Also), Then, Else, Else If, End If and handling errors with stacktrace during the flow.

You can transform the if-or-or condition groups of a flow into if-or-finally or try-catch-finally.

Furthermore there is a Flow Variables device, granting variable management/memory conditions for manipulating variables during flows, with expression for which you can define your own custom functions in the settings.

And again, please take a look at the Community Forum Topic before usages.

Please report issues on the issues site.