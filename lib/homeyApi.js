const defaultId = "__def_id"
const _ = require('lodash');
const { Defer } = require("./proto");
//const {HomeyAPI} = require('athom-api');
//const Script = require('./script');

class homeyApi {
    
    static get ready() { if(!this._ready) this._ready= new Defer(); return this._ready.promise;}

    static init(app) {
        if(this.app) return;
        if(app) {
            this.app = app;
            this.homey = app.homey;
            this.log=app.log.bind(app);
            this.error=app.error.bind(app);
            //this.localURL = app.localURL;
        }
        if(!this.ready); //Initialize the defer

        // this.clientSettings = {
        //     clientId: process.env.ATHOM_API_CLIENT_ID,
        //     clientSecret: process.env.ATHOM_API_CLIENT_SECRET,
        //     redirectUrl: process.env.ATHOM_API_LOGIN_URL
        // }
        //this.homey.settings.set('', this.clientSettings)
    }


    // static async getHomeyAPI(id) {
    //     id = id || defaultId;
	// 	if(!this.apis) this.apis = {};
	// 	if(!this.apis[id]) this.apis[id] = await HomeyAPI.forCurrentHomey(this.homey);
	// 	return this.apis[id];
    // }
    // static async refreshHomeyAPI(id) {
    //     id = id || defaultId;	
	// 	if(!this.apis) this.apis = {};
	// 	if(true || !this.apis[id]) this.apis[id] = await HomeyAPI.forCurrentHomey(this.homey);
	// 	return this.apis[id];
    // }
	static async clearHomeyAPI(id) {
		if(id && this.apis) delete this.apis[id];
    }

    /** @returns {import('athom-api').HomeyAPI} */
    static async getHomeyAPI(id) {
        id = id || defaultId;
		if(!this.apis) this.apis = {};
		if(!this.apis[id]) this.apis[id] = await this.getAsync();
		return this.apis[id];
    }
    
    /** @returns {import('athom-api').HomeyAPI} */
    static async refreshHomeyAPI(id) {
        id = id || defaultId;	
		if(!this.apis) this.apis = {};
		if(true || !this.apis[id]) this.apis[id] = await this.getAsync();
		return this.apis[id];
    }
    /** @returns {import('athom-api').HomeyAPI} */
    static get() {
        return this.homey.drivers.getDriver('homeyelevator').homeyApi;
    }
    /** @returns {Promise<import('athom-api').HomeyAPI>} */
    static async getAsync() {
        var client = null;
        try {            
            client = this.homey.drivers.getDriver('homeyelevator').homeyApi;            
        } catch (error) { }
        if(!client) {
            await this.homey.drivers.getDriver('homeyelevator').ready();
            await this.ready;

            client = this.homey.drivers.getDriver('homeyelevator').homeyApi;
        }

        return client;

        // if(this.token && this.token.then) return this.token;
        // var defer = new Defer();
        // if(!this.homeyApi) {
        //     this.token = defer.promise;
            
        //     Script.getToken().then(async (token)=> {
        //         this.token=token;                
        //         this.log('token', token);
        //         try {
        //             let r= await HomeyAPI.forCurrentHomey(this.homey, this.token);//await this.getHomeyAPINew(this.token);//forCurrentHomey(this.homey, this.token);
        //             this.homeyApi = r;
        //             defer.resolve(r);
                    
        //             this.log('defer resolved', r);
        //             return ;
        //         } catch (error) {
        //             this.token = await this.homey.drivers.getDriver('homeyelevator').getToken();
        //             let r= await HomeyAPI.forCurrentHomey(this.homey, this.token);
        //             this.homeyApi = r;
        //             return defer.resolve(r);//HomeyAPI.forCurrentHomey(this.homey, this.token));
        //         }   
        //     });
        // } else defer.resolve(this.homeyApi);
        // return defer.promise;    
    }



    static async getFlows(refresh) {
        if(!this.flows || this.flows.length==0 || refresh)  {
            var api = await this.getHomeyAPI();
            if(!api || !api.flow) api = await this.refreshHomeyAPI();
            this.flows = await api.flow.getFlows({$skipCache: true });     
        } 
        this.log('getFlows', typeof(this.flows));
        return this.flows;
    }
    static async getFlowCards({refresh, type}) {
        if(!this.flowCards || refresh) 
            var api = await this.getHomeyAPI();
            if(!api || !api.flow) api = await this.refreshHomeyAPI();
            this.flowCards = await api.flow[type=='actions' ? 'getFlowCardActions' : type=='conditions' ? 'getFlowCardConditions' : 'getFlowCardTriggers']();            
        return this.flowCards;
    }
    static async getFlowCard({uri, id, type}) {
        var api = await this.getHomeyAPI();
        if(!api || !api.flow) api = await this.refreshHomeyAPI();

        return await api.flow[type=='actions' ? 'getFlowCardAction' : type=='conditions' ? 'getFlowCardCondition' : 'getFlowCardTrigger']({uri, id, $skipCache: true});
    }

    static async getFlow({id}) {
       // var api = (await this.refreshHomeyAPI());
        return (await this.refreshHomeyAPI()).flow.getFlow({id:id, $skipCache: true});
    }
    static async getDevice({id}) {
        return await (await this.getHomeyAPI()).devices.getDevice({id});
        //return _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> x.id==id);//(await this.refreshHomeyAPI()).flow.getFlow({id:id});
    }
    static async getDevices() {
        return await (await this.getHomeyAPI()).devices.getDevices();
        //return _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> x.id==id);//(await this.refreshHomeyAPI()).flow.getFlow({id:id});
    }
    static async setDeviceName({id, name}) {
        return await (await this.getHomeyAPI()).devices.updateDevice({id, device:{name}});
        //return _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> x.id==id);//(await this.refreshHomeyAPI()).flow.getFlow({id:id});
    }

    static async getOwnTriggerFlows({refresh=false, device, args, triggerCardId, cardId, overloadable}) {
        if(device) {
            device = _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> device.id ? x.id==device.id : x.data.id== device.getData().id);
        }
        if(device&&device.settings && overloadable!==true && overloadable!==false)overloadable = device.settings.overloading=='true';
        if(true || !this.ownFlows || !this.ownFlows.length || refresh) {
            var flows = await this.getFlows(true); //refresh
            this.ownFlows = flows ? _.filter(flows, x=> x.trigger.uri==(device? 'homey:device:'+ device.id : 'homey:app:nl.qluster-it.HOOP') ) : null;
        }        
        if(!this.ownFlows) return null;
        var ownFlows = _.filter(this.ownFlows, x=> overloadable || !cardId || x.trigger.id.split('__')[1]==cardId );
        
        //&& (overloadable || !cardId || x.trigger.id.split('__')[1]==cardId )
        
        if(args && Object.keys(args).length > 0) {
            var toFindArgsCount = {};
            for (let argI = 0; argI < args.length; argI++) {
                const arg = args[argI];
                if(!arg.name.startsWith('text_') && !arg.name.startsWith('number_') && !arg.name.startsWith('boolean_')) continue;
                var argType = arg.name.split('_')[0];
                var argNumber = Number.parseInt(arg.name.split('_')[1]);
                if(toFindArgsCount[argType]===undefined) toFindArgsCount[argType] = argNumber;
                else if(argNumber>toFindArgsCount[argType]) toFindArgsCount[argType] = argNumber;
            }
            var toFindArgs = {};
            _.forEach(toFindArgsCount, (f, key)=> {
                if(!toFindArgs[key]) toFindArgs[key] = [];
                for (let i = f; i <= 3; i++) 
                    toFindArgs[key].push(key + i);
                
            });
            ownFlows = _.filter(ownFlows, f=>  _.every(toFindArgs, toFindArg=> _.some(toFindArg, toFindArgName=>  {
                return f.trigger.id.indexOf(toFindArgName)>-1;
            }) ));
        }
        //this.log('ownFlows', ownFlows);
        if(triggerCardId) ownFlows = _.filter(ownFlows, f=>f.trigger.id==triggerCardId);
        return ownFlows;
    }


    
    // static async getFlows(refresh) {
    //     if(!this.flows || refresh)  {
    //         var api = await this.getHomeyAPI();
    //         if(!api || !api.flow) api = await this.refreshHomeyAPI();
    //         this.flows = await api.flow.getFlows({$skipCache: true });     
    //     }       
    //     return this.flows;
    // }
    // static async getFlowCards({refresh, type}) {
    //     if(!this.flowCards || refresh) 
    //         var api = await this.getHomeyAPI();
    //         if(!api || !api.flow) api = await this.refreshHomeyAPI();
    //         this.flowCards = await api.flow[type=='actions' ? 'getFlowCardActions' : type=='conditions' ? 'getFlowCardConditions' : 'getFlowCardTriggers']();            
    //     return this.flowCards;
    // }
    // static async getFlowCard({uri, id, type}) {
    //     var api = await this.getHomeyAPI();
    //     if(!api || !api.flow) api = await this.refreshHomeyAPI();

    //     return await api.flow[type=='actions' ? 'getFlowCardAction' : type=='conditions' ? 'getFlowCardCondition' : 'getFlowCardTrigger']({uri, id, $skipCache: true});
    // }

    // static async getFlow({id}) {
    //     return (await this.refreshHomeyAPI()).flow.getFlow({id:id, $skipCache: true});
    // }
    // static async getDevice({id}) {
    //     return await (await this.getHomeyAPI()).devices.getDevice({id});
    //     //return _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> x.id==id);//(await this.refreshHomeyAPI()).flow.getFlow({id:id});
    // }

    // static async getOwnTriggerFlows({refresh=false, device, args, triggerCardId, cardId, overloadable=false}) {
    //     if(device) {
    //         device = _.find(await (await this.getHomeyAPI()).devices.getDevices(), x=> device.id ? x.id==device.id : x.data.id== device.getData().id);
    //     }

    //     if(!this.ownFlows || !this.ownFlows.length || refresh) {
    //         var flows = await this.getFlows(refresh);
    //         this.ownFlows = flows ? _.filter(flows, x=> x.trigger.uri==(device? 'homey:device:'+ device.id : 'homey:app:nl.qluster-it.FlowEnhancer') ) : null            
    //     }        
    //     if(!this.ownFlows) return null;
    //     var ownFlows = _.filter(this.ownFlows, x=> overloadable || !cardId || x.trigger.id.split('__')[1]==cardId );
        
    //     //&& (overloadable || !cardId || x.trigger.id.split('__')[1]==cardId )
        
    //     if(args && Object.keys(args).length > 0) {
    //         var toFindArgsCount = {};
    //         for (let argI = 0; argI < args.length; argI++) {
    //             const arg = args[argI];
    //             if(!arg.name.startsWith('text_') && !arg.name.startsWith('number_') && !arg.name.startsWith('boolean_')) continue;
    //             var argType = arg.name.split('_')[0];
    //             var argNumber = Number.parseInt(arg.name.split('_')[1]);
    //             if(toFindArgsCount[argType]===undefined) toFindArgsCount[argType] = argNumber;
    //             else if(argNumber>toFindArgsCount[argType]) toFindArgsCount[argType] = argNumber;
    //         }
    //         var toFindArgs = {};
    //         _.forEach(toFindArgsCount, (f, key)=> {
    //             if(!toFindArgs[key]) toFindArgs[key] = [];
    //             for (let i = f; i <= 3; i++) 
    //                 toFindArgs[key].push(key + i);
                
    //         });
    //         ownFlows = _.filter(ownFlows, f=>  _.every(toFindArgs, toFindArg=> _.some(toFindArg, toFindArgName=>  {
    //             return f.trigger.id.indexOf(toFindArgName)>-1
    //         }) ))
    //     }
    //     //this.log('ownFlows', ownFlows);
    //     if(triggerCardId) ownFlows = _.filter(ownFlows, f=>f.trigger.id==triggerCardId);
    //     return ownFlows;
    // }
}


module.exports = homeyApi;